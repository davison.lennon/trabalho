#!/usr/bin/python3

# server.py: Servidor que processa a conversão de unidades de medidas
# Uso: python3 server.py [host (opcional)] [porta (opcional)]

# Inclusão de módulos
from datetime import datetime   # Para tratar data e hora
import socket   # Para utilizar as funções de socket

import os   # Para uso de recursos do sistema operacional (o.s.)
import sys  # Para a leitura de paramentros da linha de comando

# Linhas necessárias para importar o modulo utils.py no MS Windows
localpath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(localpath)

from utils import * # Módulo que controla as cores da tela


# Funções auxiliares
def agora():    # Criar um prompt com data e hora atuais
    agora = datetime.now().strftime("%d/%m/%Y %H:%M:%S -> ")
    print("\n" + color("cyan") + agora + normalmode(), end="")

# Converte as medidas 
def conversor(grandeza, medida_origem, medida_destino, valor):
    # Prefixos:
    # 0 -> k  (quilo)
    # 1 -> h  (hecto)
    # 2 -> da (deca)
    # 3 -> string vazia (elemento neutro da concatenação de strings)
    # 4 -> d  (deci)
    # 5 -> c  (centi)
    # 6 -> m  (mili)
    prefixos = ["k", "h", "da", "", "d", "c", "m"]
    
    # Unidade de medida base (ou fundamental)
    # 0 -> string vazia (elemento neutro da concatenação de strings)
    # 1 -> m  (metro)
    # 2 -> m² (metro quadrado)
    # 3 -> m³ (metro cúbico)
    # 4 -> l  (litro)
    # 5 -> g  (grama)
    base = ["", "m", "m²", "m³", "l", "g"]

    saida = ""
    exp = 1 # Fator multiplicativo para grandezas unidimensionais
    if grandeza == 2 or grandeza == 3:
        exp = grandeza # Fator multiplicativo para grandezas
                       # bi ou tridimensionais
    
    # Conversão de medida
    valor_convertido = valor * 10**(exp * (medida_destino - medida_origem))
    
    # Criando a saída formatada com os valores original e convertido
    # com suas respectivas unidades de medida
    saida = saida + str(valor) + " " + prefixos[medida_origem] + base[grandeza]

    saida = saida + " ===> " 

    saida = saida + str(valor_convertido) + " " + prefixos[medida_destino] + base[grandeza]
        
    return saida # A função retorna a saída formatada


# Servidor
def servidor(host="localhost", port=2022):
    # Criando um socket TCP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Habilitando o reuso de endereço e porta (não mudam)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind (preparação para a ligação)
    agora() # Exibe data e hora atualizadas
    print("Iniciando o servidor em " + color("magenta") + host + normalmode() + ":" + color("yellow") + str(port) + normalmode() + ".")
    
    s.bind((host, port)) # A ligação de fato ( a função bind() recebe
                         # uma tupla formada por host e port
    n_max = 10  # Número máximo de conexões simultâneas
    s.listen(n_max) # O servidor "escuta" os clientes

    # Aceitando o cliente
    agora()
    print("Aguradando conexão como cliente.")
    cliente, endereco = s.accept() # Conexão aceita

    agora()
    print("Cliente conectado.")

    try:
        while True: # Executa o laço while sempre (não requer nova conexão)
            agora()
            print("Aguardando mensagem do cliente.")
            color("yellow")
            print("Pressione CTRL+C para encerrar o servidor.")
            normalmode()

            # Recebimento de dados do cliente
            dados = cliente.recv(2048) # Agurada dados do cliente
                                       # Cliente envia 2048 bits

            if not dados:
                agora()
                print("Aguardando conexão com o cliente.")
                cliente, endereco = s.accept()

                agora()
                print("Cliente conectado.")
            else:
                # Decodifficando os dados recebidos
                dados = dados.decode()
                # Separando os campos de dados
                campos = dados.split("@")

                # definindo o que significa cada campo de dado
                grandeza = int(campos[0])
                medida_origem = int(campos[1])
                medida_destino = int(campos[2])
                valor = float(campos[3])
                
                # Exibindo as imformações na tela
                agora()
                print("Dados recebidos do cliente:")
                color("green")
                print("\t* Grandeza = " + str(grandeza))
                print("\t* Medida de origem = " + str(medida_origem))
                print("\t* Medida de destino = " + str(medida_destino))
                print("\t* Valor = " + str(valor))
                normalmode()

                # Efetuando a conversão e retornando a mensagem formatada
                mensagem = conversor(grandeza, medida_origem, medida_destino, valor)

                # Exibindo mensagem
                agora()
                print("Mensagem: " + color("magenta") + mensagem +  normalmode())

                agora()
                color("yellow")
                print("Retornando mensagem para o cliente.")
                normalmode()

                # Mensagem codificada e enviada ao cliente
                cliente.send(mensagem.encode("utf-8"))

                agora()
                print("Mensagem envidada.")

    except socket.error as e:
        agora()
        print("Erro de socket: %s" % str(e))

    except Exception as e:
        agora()
        print("Outra exceção: %s" % str(e))

    finally:
        agora()
        print("Encerrando conexão.")
        s.close()
        return

# Executar o servidor com parâmetros
if __name__ == "__main__":
    n_args = len(sys.argv) - 1

    if n_args == 0:
        servidor()
    elif n_args == 1:
        servidor(sys.argv[1])
    elif n_args == 2:
        servidor(sys.argv[1], int(sys.argv[2]))
