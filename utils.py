import sys
import os

class DotDict(object):
    def __init__(self, **kwargs):
        self._dict = dict(**kwargs)

    def __getattr__(self, name):
        return self._dict.get(name)

    def __getitem__(self, name):
        return self._dict[name]



Fg = DotDict(
        BLACK = "\033[30m",
        RED = "\033[31m",
        GREEN = "\033[32m",
        YELLOW = "\033[33m",
        BLUE = "\033[34m",
        MAGENTA = "\033[35m",
        CYAN = "\033[36m",
        WHITE = "\033[37m"
    )

Bg = DotDict(
        BLACK = "\033[40m",
        RED = "\033[41m",
        GREEN = "\033[42m",
        YELLOW = "\033[43m",
        BLUE = "\033[44m",
        MAGENTA = "\033[45m",
        CYAN = "\033[46m",
        WHITE = "\033[47m"
    )

Style = DotDict(
        RESET = "\033[0m"
    )


def color(color_name):
    if os.name == "posix":
        colors_list = {
                "black" : "30",
                "red" : "31",
                "green" : "32",
                "yellow" : "33",
                "blue" : "34",
                "magenta" : "35",
                "cyan" : "36",
                "white" : "37"
            }

        code = "\033[" + colors_list[color_name] + "m"
        print(code, end = "")

        return code
    else:
        return ""

def normalmode():
    if os.name == "posix":
        code = "\033[0m"
        print(code, end = "")

        return code
    else:
        return ""

def clear():
    if os.name == "posix":
        code = "\033[2J\033[1;1H"
        print(code, end = "")
    elif os.name == "nt":
        os.system("CLS")

#print(f"Isso é um {Fg.YELLOW}teste{Style.RESET} de cores {Bg.BLUE}muito{Style.RESET} interessante.")
