#!/usr/bin/python3

# cliente.py: Interface de interação humana para a conversão de unidade medida.
# Uso: python3 cliente.py [host (opcional)] [porta (opcional)]

# Inclusão de módulos
import socket # Para utilizar as funções de socket

import os   # Para uso de recursos do sistema operacional (o.s.)
import sys  # Para a leitura de parâmetros da linha de comando.

# Linhas necessárias para importar o módulo utils.py no MS Windows
localpath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(localpath)

from utils import * # Módulo que controla as cores da tela


# Funções auxiliares
def e_decimal(str): # Verifica se um string é um número decimal
    try:
        float(str) # Tenta converter string para número de ponto flutuante
        return True # Se não ocorre erro, então retorna True
    except:
        return False # Caso contrario, retorna False


def menu_grandezas(): # Menu ue apresenta as grandezas
    clear()
    print("====== GRANDEZAS ======")
    print("\t1 - Comprimento")
    print("\t2 - Área")
    print("\t3 - Volume")
    print("\t4 - Capacidade")
    print("\t5 - Massa")

    return int(input("Escolha a grandeza: ")) # retorna número
                                              # do item

def menu_medidas(grandeza):
        medidas = []
        if grandeza == 1:
            medidas = [
                    "0 - Quilômetro (km)",
                    "1 - Hectômetro (hm)",
                    "2 - Decâmetro (dam)",
                    "3 - Metro (m)",
                    "4 - Decímetro (dm)",
                    "5 - Centímetro (cm)", 
                    "6 - Milímetro (mm)"
                ]

        if grandeza == 2:
            medidas = [
                    "0 - Quilômetro quadrado (km²)",
                    "1 - Hectômetro quadrado (hm²)",
                    "2 - Decâmetro quadrado (dam²)",
                    "3 - Metro quadrado (m²)",
                    "4 - Decímetro quadrado (dm²)",
                    "5 - Centímetro quadrado (cm²)",
                    "6 - Milímetro quadrado (mm²)"
                ]

        if grandeza == 3:
            medidas = [
                    "0 - Quilômetro cúbico (km³)",
                    "1 - Hectômetro cúbico (hm³)",
                    "2 - Decâmetro cúbico (dam³)",
                    "3 - Metro cúbico (m³)",
                    "4 - Decímetro cúbico (dm³)",
                    "5 - Centímetro cúbico (cm³)",
                    "6 - Milímetro cúbico (mm³)"
                ]

        if grandeza == 4:
            medidas = [
                    "0 - Quilolitro (kl)",
                    "1 - Hectolitro (hl)",
                    "2 - Decalitro (dal)",
                    "3 - Litro (l)",
                    "4 - Decilitro (dl)",
                    "5 - Centilitro (cl)",
                    "6 - Mililitro (ml)"
                ]

        if grandeza == 5:
            medidas = [
                    "0 - Quilograma (kg)",
                    "1 - Hectgrama (hg)",
                    "2 - Decagrama (dag)",
                    "3 - Grama (g)",
                    "4 - Decigrama (dg)",
                    "5 - Centigrama (cg)",
                    "6 - Miligrama (mg)"
                ]

        print("\n+++++ MEDIDAS +++++")
        for m in medidas:
            print(m)

        m1 = input("Escolha a medida de origem: ")
        m2 = input("Escolha a medida de destino: ")
        v = input("Insira o valor: ")

        msg = str(grandeza) + "@" + m1 + "@" + m2 + "@" + v
        
        return msg


# Cliente
def cliente(host = "localhost", port = 2022):
    # Criando um socket TCP/IP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Conectando socket ao servidor
        print("Conectando no endereço %s na porta %s." % (host, port))
        s.connect((host, port))
        print("Conectado ao servidor.")

    except socket.error as e: # Erros de socket
        print("Erro de socket: %s" % str(e))
        print("Fechando a conexão.")
        s.close()
        return

    # Enviando dados
    try:
        while True: # Cliente funcionando em loop infinito
            print("Pressione CTRL-C ou CTRL-D para encerrar o cliente.")
            
            mensagem = menu_medidas(menu_grandezas())

            # Enviando mensagem ao servidor
            # print("\nEnviando a mensagem [%s]" % mensagem)
            s.sendall(mensagem.encode("utf-8")) # Codifica mensagem

            # Recebendo a mensagem do servidor contendo o IMC
            mensagem = s.recv(2048)
            
            print("\nResposta enviada pelo servidor:")
            resposta = mensagem.decode()
            print("\t»»» " + resposta)

            input("\n\nPressione ENTER para efeturar uma nova conversão ou CTRL+D para encerrar")

    except socket.error as e: # Erro de socket
        print("Erro de socket: %s" % str(e))

    except Exception as e: # Erro diversos
        print("Outras exceção: %s" % str(e))

    finally: # Finaliza a conxão e encerra o programa
        print("Encerrando conexão.")
        s.close() # Fecha socket
        return

# Executar cliente
if __name__ == "__main__":
    n_args = len(sys.argv) - 1 # Conta os argumentos da linha de comando

    if n_args == 0: # Zero argumentos
        cliente() # parâmetros padrão: cliente(host="localhost", port = 2022)
    elif n_args == 1: # Um argumento
        cliente(sys.argv[1]) # Usa host fornecido na linha de comando e porta padrão
    elif n_args == 2: # Dois argumentos
        cliente(sys.argv[1], int(sys.argv[2])) # Usa host e porta fornecidos na linha de comando

