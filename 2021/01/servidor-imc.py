#!/usr/bin/python

from datetime import datetime
import sys
import socket

def servidor(host="localhost", port=2021):
    # Criando um socket TCP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Habilitando reuso de endereço e porta
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # Bind
    agora()
    print("Iniciando o servidor em \033[33m%s\033[0m:\033[31m%s\033[0m." % (host,port))
    s.bind((host,port))
    # Escutando clientes com n_max conexoes simultâneas
    n_max = 10
    s.listen(n_max)

    # Aceitando cliente
    agora()
    print("Aguardando conexão com o cliente.")
    cliente, endereco = s.accept()
    agora()
    print("Cliente conectado.")

    try:
        while True:
            agora()
            print("Aguardando mensagem do cliente.")
            print("Pressione CTRL+C para encerrar o servidor.")
            # Recebimento de dados do cliente
            dados = cliente.recv(2048)

            if not dados: # Se não há dados
                agora()
                print("Aguardando conexão com o cliente.")
                cliente, endereco = s.accept()
                agora()
                print("Cliente conectado.")

            else:
                # Exibição dos dados
                dados = dados.decode()
                valor = dados.split(":")
            
                massa = float(valor[0])
                altura = float(valor[1])
                print("\n\033[34m»»» [Massa = %s kg, Altura = %s m]\033[0m" % (massa, altura))

                # Obtendo o valor do IMC
                resposta = imc(massa, altura)
                print("\033[35m»»» Resposta: [IMC = %f kg/m²]\n\033[0m" % resposta)

                # Enviando mensagem para o cliente
                mensagem = str(resposta)
                cliente.send(mensagem.encode("utf-8"))
                agora()
                print("Enviados %s bytes para \033[33m%s\033[0m:\033[31m%s\033[0m.\n" % (len(mensagem), endereco[0], endereco[1]))
               
    except socket.error as e:
        agora()
        print("Erro de socket: %s." % str(e))

    except Exception as e:
        agora()
        print("Outra exceção: %s." % str(e))

    finally:
        agora()
        print("Encerrando conexão.")
        s.close()
        return


# Função para o cálculo do IMC
def imc(massa, altura):
    return massa / altura**2

# Imprime data e hora atualizadas
def agora():
    agora = datetime.now().strftime("%d/%m/%Y %H:%M:%S → ")
    print("\033[36m%s\033[0m" % agora, end="")

# Executar o servidor
if __name__ == "__main__":
    n_args = len(sys.argv)

    if n_args == 1:
        servidor()
    elif n_args == 2:
        servidor(sys.argv[1])
    elif n_args == 3:
        servidor(sys.argv[1],int(sys.argv[2]))

