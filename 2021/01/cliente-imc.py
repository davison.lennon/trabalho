#!/usr/bin/python

import sys
import socket

def cliente(host = "localhost", port = 2021):
    # Criando um socket TCP/IP
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Conectando socket ao servidor
        print("Conectando em %s na porta %s" % (host, port))
        s.connect((host, port))
        print("Conectado ao servidor.")
    
    except socket.error as e:
        print("Erro de socket: %s." % str(e))
        print("Fechando a conexão.")
        s.close()
        return

    # Enviando dados
    try:
        while True:
            print("Pressione CTRL+D ou CTRL+C para encerrar o cliente.")
            # Obtendo dados da entrada padrão
            massa = ""
            repetiu = False
            while not e_decimal(massa):
                if repetiu:
                    print("Use um número decimal para a massa.")
                massa = input("\033[32mInsira a massa (em kg): \033[0m")
                repetiu = True

            altura = ""
            repetiu = False
            while not e_decimal(altura):
                if repetiu:
                    print("Use um número decimal para a altura.")
                altura = input("\033[32mInsira a altura (em m): \033[0m")
                repetiu = True

            # Enviando mensagem ao servidor
            mensagem = massa + ":" + altura
            print("\nEnviando a mensagem [%s]" % mensagem)
            s.sendall(mensagem.encode("utf-8"))

            # Recebendo a mensagem do servidor contendo o IMC
            mensagem = s.recv(2048)
            # Extraindo o IMC da mensagem recebida
            imc = float(mensagem.decode())

            # Obtendo análise do IMC
            resposta = analisar_imc(imc)
            # Exibindo resposta
            print("\n\033[34m»» IMC = " + f"{imc:.1f}" + " kg/m² [" + str(resposta) + "]\033[0m\n")

    except socket.error as e:
        print("Erro de socket: %s" % str(e))
        
    except Exception as e :
        print("Outra exceção: %s" % str(e))

    finally:
        print("Encerrando conexão.")
        s.close()
        return

# Função para análise de IMC
def analisar_imc(imc):
    if imc < 18.5:
        return "Abaixo do peso."
    elif imc >= 18.5 and imc < 25.0:
        return "Peso normal."
    elif imc >= 25.0 and imc < 30.0:
        return "Sobrepeso."
    elif imc >= 30.0 and imc < 35.0:
        return "Obesidade grau 1."
    elif imc >= 35.0 and imc < 40.0:
        return "Obesidade grau 2."
    elif imc >= 40.0:
        return "Obesidade grau 3."

# Verificar se um string é um número em ponto flutuante
def e_decimal(str):
    try:
        float(str)
        return True
    except:
        return False

# Executar cliente
if __name__ == "__main__":
    n_args = len(sys.argv)

    if n_args == 1:
        cliente()
    elif n_args == 2:
        cliente(sys.argv[1])
    elif n_args == 3:
        cliente(sys.argv[1],sys.argv[2])
